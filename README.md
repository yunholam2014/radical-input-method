# 引言
一直以來，很多人雖然有字典在手，但卻沒有熟悉部首，自互聯網普及後情況更明顯。學生及用家更常使用網頁直接搜尋該字，而非從傳統的方式，導致他們了解字的輸入碼更甚於字的部首，此輸入法的製作目的就是讓用家重新認識每個中文正體字的部首和重拾以前查字典的樂趣。特別注意的是這個輸入法並非以效率為先，所以打得慢是很正常的。
# Introduction
People do not understand the Chinese radical system especially the universal of internet. The purpose of this input method is re-understand the radical system and the feeling of checking dictionary. And Radical Input Method is not efficiency, but this is normal.
# 部首
中文字歸類於語素文字，非像其他文字，如日語、英語等，可用字的第一個發音或字母進行分類。為將中文分類，由東漢文字學家許慎(約公元100年)的《說文解字》創出部首這個系統，經過差不多兩千年的語文改變及系統改善，現今所通用的部首是根據明朝國子監太學生梅膺祚(約公元1615年)所著的《字彙》中提出的214個部首(詳見附件一)。
比較特別的是部首在文字中時並不一定是原本部首的樣子和筆劃數，例如「辵」這個部首在文字中顯示是「⻍」，「阜」就顯示為「阝」等等。另一類較有趣的文字是文字本身的筆劃數比其部首的筆劃數更少，例如「王」的部首是「玉」。
# Radical
Radical system is a traditional classification of Chinese characters since A.D.100, and the modern radical system is created in A.D.1615 which is only 214 radicals.
# 筆劃
除了用部首分類，大量的中文字亦需用筆劃數目去分類。然而某些字的筆劃數是有爭議的，因為部分人書寫時未必依照「正宗」的寫法，所以認識字筆劃數能有助減少寫錯字的機會。
# Stroke
Chinese characters also classified by stroke, but it is controversial situation about some quantity of stroke, because people may not write Chinese characters with "original" order of strokes.
# 部首輸入法
使用部首輸入法時就有如小時候查字典的過程，共分兩個部分：部首筆劃和部件筆劃。頭兩個數字為部首的筆劃數目，後二至五個數字為部件筆劃數。以「部」字做例，其部首是「邑」共7劃，所以頭兩個數字碼為「07」；除去部首後剩下的「咅」是8劃，所以後兩個數字是「08」。因為「部」字用部首輸入法時的輸入碼為「0708」。
但有些文字是需要在四個數字後加上一至三個「」，原因在於輸入法的限制：每個輸入碼只可提供最多一百個文字選項。而有些輸入法有超過一百，甚至三百個選項（詳見附件二），所以用四個數字後加上「」以作文字選項列表的延伸。
# Radical Input Method
The input code of Radical Input Method divided into two sections: quantity of radical, quantity of parts. First two numbers are the strokes of radical, and last two to five numbers are the strokes of parts. For example, the radical of 「部」is 「阜」which have seven strokes and the parts 「咅」have eight strokes, so the input code of 「部」is "0708".
However, some input code need to add one to three "" key after those four numbers because of the limitation of input method: each input code could only show 100 candidates. And some input code have over 100, even 300 candidates (see Appendix II), therefore the "" key becomes the extension of candidate table.
# 限制
每個輸入碼只可提供最多一百個文字選項（詳見「部首輸入法」），但同一輸入碼的數目太多（詳見附件二）。
基於部分人學習中文正體字的途徑不同，引致某些字在缺乏統一標準下出現筆劃數目的爭議。
因為輸入法所用的關鍵輸入按鈕是「0123456789*」，所以不能用傳統的1~9去選擇文字，取而代之是使用F1~F9。
# Limitations
Each input code could only show 100 candidates (see "Radical Input Method"), but there are many input code have over 100 candidates (see Appendix II).
There is no standard to court strokes because of different learning of Chinese Language, so it may have some conflict between some Chinese characters.
It requires F1 to F9 key to choose candidates, since the key select buttons are "0123456789*".
# 附件一
# Appendix I
## 一劃
一、丨、丶、丿、 乙、亅
## One stroke
one, line, dot, slash, second, hook
## 兩劃
二、亠、人、儿、入、八、冂、冖、冫、几、凵、刀、力、勹、匕、匚、匸、十、卜、卩、厂、厶、又
## Two strokes
two, lid, man, legs, enter, eight, wide, cloth cover, ice, table, receptacle, knife, power, wrap, spoon, box, hiding, ten, divination, seal, cliff, private, again
## 三劃
口、囗、土、士、夂、夊、夕、大、女、子、宀、寸、小、尢、尸、屮、山、巛、工、己、巾、干、幺、广、廴、廾、弋、弓、彐、彡、彳
## Three strokes
mouth, enclosure, earth, scholar, go, go slowly, evening, big, woman, child, roof, inch, small, lame, corpse, sprout, mountain, river, work, oneself, turban, dry, short thread, dotted cliff, long stride, arch, shoot, bow, snout, bristle, step
## 四劃
心、戈、戶、手、支、攴、文、斗、斤、方、无、日、曰、月、木、欠、止、歹、殳、毋、比、毛、氏、气、水、火、爪、父、爻、爿、片、牙、牛、犬
## Four strokes
heart, halberd, door, hand, branch, rap, script, dipper, axe, square, not, sun, say, moon, tree, lack, stop, death, weapon, do not, compare, fur, clan, steam, water, fire, claw, father, trigrams, split wood, slice, fang, cow, dog
## 五劃
玄、玉、瓜、瓦、甘、生、用、田、疋、疒、癶、白、皮、皿、目、矛、矢、石、示、禸、禾、穴、立
## Five strokes
profound, jade, melon, tile, sweet, life, use, field, bolt of cloth, sickness, footsteps, white, skin, dish, eye, spear, arrow, stone, spirit, track, grain, cave, stand
## 六劃
竹、米、糸、缶、网、羊、羽、老、而、耒、耳、聿、肉、臣、自、至、臼、舌、舛、舟、艮、色、艸、虍、虫、血、行、衣、襾
## Six strokes
bamboo, rice, silk, jar, net, sheep, feather, old, and, plow, ear, brush, meat, minister, self, arrive, mortar, tongue, oppose, boat, stopping, color, grass, tiger, insect, blood, enclosure, clothes, cover
## 七劃
見、角、言、谷、豆、豕、豸、貝、赤、走、足、身、車、辛、辰、辵、邑、酉、釆、里
## Seven strokes
see, horn, speech, valley, bean, pig, badger, shell, red, run, foot, body, cart, bitter, morning, walk, city, wine, distinguish, village
## 八劃
金、長、門、阜、隶、隹、雨、青、非
## Eight strokes
gold, long, gate, mound, slave, short-tailed, rain, blue, wrong
## 九劃
面、革、韋、韭、音、頁、風、飛、食、首、香
## Nine strokes
face, leather, tanned, leek, sound, leaf, wind, fly, eat, head, fragrant
## 十劃
馬、骨、高、髟、鬥、鬯、鬲、鬼
## Ten strokes
horse, bone, tall, hair, fight, sacrificial, cauldron, ghost
## 十一劃
魚、鳥、鹵、鹿、麥、麻
## Eleven strokes
fish, bird, salt, deer, wheat, hemp
## 十二劃
黃、黍、黑、黹
## Twelve strokes
yellow, millet, black, embroidery
## 十三劃
黽、鼎、鼓、鼠
## Thirteen strokes
frog, tripod, drum, rat
## 十四劃
鼻、齊
## Fourteen storkes
nose, even
## 十五劃
齒
## Fifteen strokes
tooth
## 十六劃
龍、龜
## Sixteen strokes
dragon, turtle
## 十七劃
龠
## Seventeen strokes
flute
# 附件二
# Appendix II
超過100個相同輸入碼的數量列表：
The list of input code with over 100 candidates:
輸入碼 input code,數量 quantity
0304,147
0305,184
0306,176
0307,151
0308,224
0309,190
0310,141
0311,151
0312,119
0404,242
0405,312
0406,279
0407,286
0408,378
0409,354
0410,264
0411,274
0412,248
0413,167
0505,147
0506,104
0507,106
0508,146
0509,110
0603,113
0604,181
0605,231
0606,197
0607,194
0608,251
0609,227
0610,201
0611,226
0612,147
0613,141
0707,113
0708,105
